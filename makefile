CONTEINER_CERT=$(c)
CONTEINER_PASS=$(p)

help:
        # набор команд для заспаковки контейнера p12
        #
        # > make unpack c=<conteiner>.p12 p=<pass>
        # на выходе:
        # private-rsa.key - приватный ключ
        # cert-x509.crt - сертификат
        #
        # > make info
        # посмотреть информацию по сертификату
        #
        # > make check
        # посмотреть дату окончания действия сертификата
        #

unpack:
        -@openssl pkcs12 -in $(CONTEINER_CERT)  -nocerts -nodes -passin pass:$(CONTEINER_PASS) | openssl rsa -out private-rsa.key
        -@openssl pkcs12 -in $(CONTEINER_CERT) -clcerts  -nodes -nokeys -passin pass:$(CONTEINER_PASS) | openssl x509 -out cert-x509.crt
        -@openssl pkcs12 -in $(CONTEINER_CERT) -cacerts -nokeys -passin pass:$(CONTEINER_PASS) | openssl x509 -out cacerts-x509.crt

info:
        -@openssl x509 -in cert-x509.crt -text

check:
        -@openssl x509 -in cert-x509.crt -noout -enddate
